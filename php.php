<?php

$servername = "localhost";
$username = "juanito";
$password = "n3gr0L1O";

$conn = new mysqli($servername, $username, $password);

if ($conn->connect_error) {
	die("connection failed: " . $conn->connect_error);
}

echo "Connected successfully";

class Usuario 
{
	function Usuario() {
		$this->avatar = null;
		$this->nombre = null;
		$this->direccion = null;
		$this->ciudad = null;
		$this->telefono = null;
		$this->email = null;
		$this->fenac = null;
		$this->lugnac = null;
		$this->genero = null;
		$this->nacionalidad = null;
		$this->estadocivil = null;
		$this->permisoconducir = null;
		$this->pagweb = null;
		$this->idioma = null;
		$this->resumen = null;
	}
}

function crear_formulario() {
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
$sql = "insert into user_data (avatar, nombres, apellidos, direccion, ciudad,
					   telefono, email, fenac, lugnac, genero, nacionalidad,
					   estadocivil, permisoconducir, pagweb, idioas, resumen)
	values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);"

	$stmt = $mysqli->prepare($sql);
	$stmt->bind_param("bsssssssssssssss",
			$_POST['avatar'],
			$_POST['nombre'],
			$_POST['direccion'],
			$_POST['ciudad'],
			$_POST['telefono'],
			$_POST['email'],
			$_POST['fenac'],
			$_POST['lugnac'],
			$_POST['genero'],
			$_POST['nacionalidad'],
			$_POST['estadocivil'],
			$_POST['permisoconducir'],
			$_POST['pagweb'],
			$_POST['idioma'],
			$_POST['resumen']);
	$stmt->execute();

	$stmt->close();
	}

	$conn->close();
}

function datos_usuario() {
$sql = "insert into usuario (username, password, user_data) 
	values (?,?,
			(select * from user_data order by user_data desc limit 0,1)
			);"

	if ($_SERVER["REQUEST_METHOD"] == "POST"){
		$stmt = $mysqli->prepare($sql);
		$stmt->bind_param("ss",$_POST["username"],
				$_POST["password"]);
		$stmt->execute();
		$stmt->close();
	}
	$conn->close();
}

function cv_completo() {
$sql = "select ud.avatar, ud.nombres, ud.apellidos, ud.direccion, ud.ciudad, ud.telefono,
	   ud.email. ud.fenac, ud.lugnac, ud.genero, ud.nacionalidad, ud.estadocivil,
	   ud.permisoconducir, ud.pagweb, ud.idiomas, ud.resumen
	   from user_data ud, usuario u
	   where u.user_data = ud.user_data and
			 u.username = ?;";
		$user = new User();

	if (_SERVER["REQUEST_METHOD"] == "POST") {
		$stmt = $mysqli->prepare($sql);
		$stmt->bind_param("s",$_POST["username"]);
		$stmt->execute();
		$result = $stmt->get_result();

		if($result->num_rows === 0) exit("no rows");
		while($row = $result->fetch_object()){
			$user = $row;
		}
		$stmt->close();
	}

	$conn->close();
	return $user;
}

function validar_usuario() {
$sql = "select * from usuario where username = ? and password = ?; "
	if (_$SERVER["REQUEST_METHOD"] == "POST"){
		$stmt = $mysqli->prepare($sql);
		$stmt->bind_param("ss",$_POST["username"],
					$_POST["password"]);
		$stmt->execute();
		$arr = $stmt->get_result()->fetch_assoc();

		if(!$arr) exit("No user");
		$stmt->close();
		return 1;
	}
	$conn->close();
}

function usuario_activo() {
	$conn->close();
}

function eliminar_datos_usuario(){
	$conn->close();
}

function base_datos_actualizada(){
	$conn->close();
}


?>
