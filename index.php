<?php
session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Curriculum</title>
    <meta name="author" content="Devuan" />
    <!-- Date: 2020-03-04 -->
    <link rel="stylesheet" href="index.css">
    <script src="jquery-3.4.1.min.js"></script>

</head>

<body id="body">


    <div>
        <form id="formulario" class="formulario" action="mux.php" method="post">

            <h2 class=formulario__titulo>Plantillas</h2>

            <div class="contenedor-plantillas">
                <div class="plantilla">
                    <label>
                        <input class="radio" name="template" type="radio" value="auckland.php">
                        <img src="Plantillas/auckland/preview.png"preview>
                        <p>Auckland</p>
                    </label>
                </div>
                <div class="plantilla">
                    <label>
                        <input class="radio" name="template" type="radio" value="berkeley.php">
                        <img src="Plantillas/berkeley/preview.png">
                        <p>Berkeley</p>
                    </label>
                </div>
                <div class="plantilla">
                    <label>
                        <input class="radio" name="template" type="radio" value="cambridge.php">
                        <img src="Plantillas/cambridge/preview.png">
                        <p>Cambridge</p>
                    </label>
                </div>
            </div>
            <div class="contenedor-plantillas">
                <div class="plantilla">
                    <label>
                        <input class="radio" name="template" type="radio" value="otago.php">
                        <img src="Plantillas/otago/preview.png">
                        <p>Otago</p>
                    </label>
                </div>
                <div class="plantilla">
                    <label>
                        <input class="radio" name="template" type="radio" value="oxford.php">
                        <img src="Plantillas/oxford/preview.png">
                        <p>Oxford</p>
                    </label>
                </div>
                <div class="plantilla">
                    <label>
                        <input class="radio" name="template" type="radio" value="princeton.php">
                        <img src="Plantillas/princeton/preview.png">
                        <p>Princeton</p>
                    </label>
                </div>
            </div>

            <h2 class="formulario__titulo">Datos Personales</h2>

            <input class="formulario__input" id="nombre" name="nombre" type="text"> <br>
            <label class="formulario__label" for="nombre">Nombre(s)</label>

            <input class="formulario__input" id="apellidos" name="apellidos" type="text"> <br>
            <label class="formulario__label" for="apellidos">Apellidos</label>

            <input class="formulario__input" id="direccion" name="direccion" type="text"> <br>
            <label class="formulario__label" for="direccion">Direccion</label>

            <input class="formulario__input" id="ciudad" name="ciudad" type="text"> <br>
            <label class="formulario__label" for="ciudad">Ciudad</label>

            <input class="formulario__input" id="telefono" name="telefono" type="text"> <br>
            <label class="formulario__label" for="telefono">Telefono</label>

            <input class="formulario__input" id="email" name="email" type="text"> <br>
            <label class="formulario__label" for="email">Email</label>

            <input class="formulario__input" id="fenac" name="fenac" type="date"> <br>
            <label class="formulario__label" for="fenac">Fecha de Nacimiento</label>

            <input class="formulario__input" id="lugnac" name="lugnac" type="text"> <br>
            <label class="formulario__label" for="lugnac">Lugar de Nacimiento</label>

            <input class="formulario__input" id="genero" name="genero" type="text"> <br>
            <label class="formulario__label" for="genero">Genero</label>

            <input class="formulario__input" id="nacionalidad" name="nacionalidad" type="text"> <br>
            <label class="formulario__label" for="nacionalidad ">Nacionalidad</label>

            <input class="formulario__input" id="estadocivil" name="estadocivil" type="text"> <br>
            <label class="formulario__label" for="estadocivil">Estado Civil</label>

            <input class="formulario__input" id="permisoconducir" name="permisoconducir" type="text"> <br>
            <label class="formulario__label" for="permisoconducir">Permiso de Conducir</label>

            <input class="formulario__input" id="pagweb" name="pagweb" type="text"> <br>
            <label class="formulario__label" for="pagweb">Página Web</label>

            <input class="formulario__input" id="idioma" name="idioma" type="text"> <br>
            <label class="formulario__label" for="idioma ">Idioma</label>

            <input class="formulario__input" id="resumen" name="resumen" type="text"> <br>
            <label class="formulario__label" for="resumen">Resumen</label>

            <input class="formulario__submit" value="Subir" type="submit">
        </form>
    </div>

</body>

</html>