<?php
session_start();
$data = $_SESSION['data'];
?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="format-detection" content="telephone=no">
    <meta http-equiv="Cache-Control" content="no-cache">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Expires" content="0">

    <link href="oxford/css.css" rel="stylesheet">

</head>

<body class="c--434a54" style="overflow: hidden !important;">

    <div class="sidebar--bg"></div>

    <section class="sections">

        <div class="section--name  has--avatar">

            <div class="section--avatar" style="background: url('https://cvmaker-storage.s3.eu-west-3.amazonaws.com/img/avatar/d723a41edf4361cd4a1a00a55a021e73eca5eaa3.jpg') no-repeat;">&nbsp;
            </div>

            <h1>Curriculum Vitae</h1>
        </div>

        <div class="section personal">
            <div class="section--title">
                <h2>
                    Personal
                </h2>
            </div>

            <ol class="section--list">
                <li class="section--item">
                    <div class="section--heading-group">
                        <div class="section--date">
                            Nombre
                        </div>
                        <div class="section--heading">
                            <h3>
                                <?=$data['n1']?> <?=$data['a1']?></h3>
                        </div>
                    </div>
                </li>
                <li class="section--item">
                    <div class="section--heading-group">
                        <div class="section--date">
                            Dirección
                        </div>
                        <div class="section--heading">
                            <h3> <?=$data['direccion']?> , <?=$data['ciudad']?></h3>
                        </div>
                    </div>
                </li>
                <li class="section--item">
                    <div class="section--heading-group">
                        <div class="section--date">
                            Número de teléfono
                        </div>
                        <div class="section--heading">
                            <h3><?=$data['telefono']?></h3>
                        </div>
                    </div>
                </li>
                <li class="section--item">
                    <div class="section--heading-group">
                        <div class="section--date">
                            Correo electrónico
                        </div>
                        <div class="section--heading">
                            <h3><?=$data['email']?>m</h3>
                        </div>
                    </div>
                </li>

                <li class="section--item">
                    <div class="section--heading-group">
                        <div class="section--date">
                            Fecha de nacimiento
                        </div>
                        <div class="section--heading">
                            <h3>
                                <?=$data['fenac']?>
                            </h3>
                        </div>
                    </div>
                </li>

                <li class="section--item">
                    <div class="section--heading-group">
                        <div class="section--date">
                            Lugar de nacimiento
                        </div>
                        <div class="section--heading">
                            <h3><?=$data['lugnac']?></h3>
                        </div>
                    </div>
                </li>

                <li class="section--item">
                    <div class="section--heading-group">
                        <div class="section--date">
                            Género
                        </div>
                        <div class="section--heading">
                            <h3> <?=$data['genero']?></h3>
                        </div>
                    </div>
                </li>

                <li class="section--item">
                    <div class="section--heading-group">
                        <div class="section--date">
                            Nacionalidad
                        </div>
                        <div class="section--heading">
                            <h3><?=$data['nacionalidad']?></h3>
                        </div>
                    </div>
                </li>

                <li class="section--item">
                    <div class="section--heading-group">
                        <div class="section--date">
                            Estado civil
                        </div>
                        <div class="section--heading">
                            <h3>
                                <?=$data['estadocivil']?></h3>
                        </div>
                    </div>
                </li>

                <li class="section--item">
                    <div class="section--heading-group">
                        <div class="section--date">
                            Permiso de conducir
                        </div>
                        <div class="section--heading">
                            <h3> <?=$data['permisoconducir']?></h3>
                        </div>
                    </div>
                </li>

                <li class="section--item">
                    <div class="section--heading-group">
                        <div class="section--date">
                            Página web
                        </div>
                        <div class="section--heading">
                            <h3><?=$data['pagweb']?></h3>
                        </div>
                    </div>
                </li>

            </ol>

        </div>

        <div class="section">

            <div class="section--title">
                <h2>
                    Perfil
                </h2>
            </div>

            <div class="section--content section--content-single-textarea">
                <p><?=$data['resumen']?>.</p>
            </div>

        </div>

        <div class="section ">

            <div class="section--title">
                <h2>
                    Experiencia laboral
                </h2>
            </div>

            <div class="section--time-line-overlay"></div>

<!--
            <ol class="section--list">
                <li class="section--item">
                    <div class="section--time-line">
                        <i class="section--time-line-dot"></i>
                    </div>
                    <div class="section--time-line-hide-overlay"></div>
                    <div class="section--heading-group">
                        <div class="section--date">sep 2018 - jul 2018</div>
                        <div class="section--heading">
                            <h3>CEO</h3>
                        </div>
                        <div class="section--sub-heading">
                            Disney, Tel Aviv </div>
                    </div>
                    <div class="section--content">
                        <p><strong>Juden </strong>y mas <strong>Juden</strong>﻿</p>
                    </div>
                </li>
            </ol>
            -->
        </div>

        <div class="section ">

            <div class="section--title">
                <h2>
                    Estudios y certificaciones
                </h2>
            </div>

            <div class="section--time-line-overlay"></div>

<!--
            <ol class="section--list">

                <li class="section--item">
                    <div class="section--time-line">
                        <i class="section--time-line-dot"></i>
                    </div>
                    <div class="section--time-line-hide-overlay"></div>
                    <div class="section--heading-group">
                        <div class="section--date">
                            sep 2018 - jul 2018
                        </div>
                        <div class="section--heading">
                            <h3>doctorado en valer verga</h3>
                        </div>
                        <div class="section--sub-heading">
                            Harvard, Chilpan </div>
                    </div>
                    <div class="section--content">
                        <p>Conocieron los tabasqueños muchas historias de Andrés Manuel López Obrador, cómo sus experiencias juveniles allá por la selva Lacandona cuando se dejó crecer el pelo y era afecto a fumar marihuana y haber sostenido relaciones sexuales
                            en sus años mozos con una yegua llamada La Canica. En pocas palabras practicaba la zoofilia, una yegua con extraordinarias grupas, propiedad de un tal tío Lucio. De acuerdo a los datos recopilados por Blas A. Buendía, el testimonio
                            periodístico del diario Tabasco al Día, fechado el 21 de mayo de 1996 en Tepetitán, Macuspana, reza en una de sus partes: "Los amores de cierto personaje de la política que se ostenta como guía moral del Partido Revolución
                            Democrática, y una soyenca yegua propiedad del tío Lucio a la que se le conoció con el mote de La Canica, es un idilio del que pormenores el compañero Azarías Mendoza Guzmán y forma parte de toda una truculenta realidad".</p>
                        <ul>
                            <li>Extracto de una nota publicada en El Diario Tabasco al Día, 21 de mayo de 1996.</li>
                        </ul>
                        <p>"Tal vez por la rabia que le ocasionó esa asquerosidad, don Lucio León, al paso de tiempo enfermó hasta que desafortunadamente, según se logró averiguar, falleció".</p>
                        <ul>
                            <li>Testimonio de un familiar de Don Lucio León "Tío Lucio"</li>
                        </ul>
                        <p>
                            <br>
                        </p>
                    </div>
                </li>
            </ol>
            -->

        </div>

        <div class="section">

            <div class="section--title">
                <h2>
                    Destrezas
                </h2>
            </div>

<!--
            <ol class="section--list skills">

                <li class="section--item skills">
                    <div class="skills--label">
                        <h3>Paja a mano cambiada</h3>
                    </div>
                    <div class="skills--stars">
                        <i class="skills--level active"></i>
                        <i class="skills--level active"></i>
                        <i class="skills--level active"></i>
                        <i class="skills--level active"></i>
                        <i class="skills--level active"></i>
                    </div>
                </li>

            </ol>
-->
        </div>

        <div class="section">

            <div class="section--title">
                <h2>
                    Idiomas
                </h2>
            </div>

            <ol class="section--list skills">

                <li class="section--item skills">
                    <div class="skills--label">
                        <h3><?=$data['idioma']?></h3>
                    </div>
                    <div class="skills--stars">
                        <i class="skills--level active"></i>
                        <i class="skills--level active"></i>
                        <i class="skills--level "></i>
                        <i class="skills--level "></i>
                        <i class="skills--level "></i>
                    </div>
                </li>

<!--
                <li class="section--item skills">
                    <div class="skills--label">
                        <h3>Grocerias</h3>
                    </div>
                    <div class="skills--stars">
                        <i class="skills--level active"></i>
                        <i class="skills--level active"></i>
                        <i class="skills--level active"></i>
                        <i class="skills--level active"></i>
                        <i class="skills--level active"></i>
                    </div>
                </li>
                -->

            </ol>

        </div>

    </section>

</body>

</html>