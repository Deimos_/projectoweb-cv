<?php
session_start();
$data = $_SESSION['data'];
?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <link href="auckland/css.css" rel="stylesheet">

</head>

<body class="c--333333" style="overflow: hidden !important;">

    <div id="avatar" class="avatar" style="background:url('https://cvmaker-storage.s3.eu-west-3.amazonaws.com/img/avatar/d723a41edf4361cd4a1a00a55a021e73eca5eaa3.jpg') no-repeat;"></div>

    <div class="sidebar--bg"></div>

    <div class="section--name" style="text-align: center;">
        <h1>
        <?=$data['n1']?> <?=$data['a1']?>
        </h1>
    </div>

    <aside class="sidebar">

        <div class="sidebar--data">
            <h2 class="sidebar--title">
                Personal
            </h2>

            <ul class="sidebar--list">
                <li class="sidebar--item sidebar--item-personal">
                    <span class="sidebar--label">Nombre</span>
                    <br> 
                    <?=$data['nombre']?> <?=$data['apellidos']?>
                </li>
                <li class="sidebar--item sidebar--item-personal">
                    <span class="sidebar--label">Dirección</span>
                    <br id="direccion"> <?= $data['direccion']?>
                    <br id="ciudad"> <?= $data['ciudad'] ?> </li>
                <li class="sidebar--item sidebar--item-personal">
                    <span class="sidebar--label">Número de teléfono</span>
                    <br id="telefono"> <?= $data['telefono'] ?>
                </li>
                <li class="sidebar--item sidebar--item-personal">
                    <span class="sidebar--label">Correo electrónico</span>
                    <br id="email"> <?= $data['email'] ?>
                </li>

                <li class="sidebar--item sidebar--item-personal">
                    <span class="sidebar--label">Fecha de nacimiento</span>
                    <br id="fenac"> <?= $data['fenac'] ?>
                </li>

                <li class="sidebar--item sidebar--item-personal">
                    <span class="sidebar--label">Lugar de nacimiento</span>
                    <br id="lugnac"> <?=$data['lugnac']?>
                </li>

                <li class="sidebar--item sidebar--item-personal">
                    <span class="sidebar--label">Género</span>
                    <br id="genero"> <?= $data['genero'] ?>
                </li>

                <li class="sidebar--item sidebar--item-personal">
                    <span class="sidebar--label">Nacionalidad</span>
                    <br id="nacionalidad"> <?= $data['nacionalidad'] ?>
                </li>

                <li class="sidebar--item sidebar--item-personal">
                    <span class="sidebar--label">Estado civil</span>
                    <br id="estadocivil"> <?= $data['estadocivil'] ?>
                </li>

                <li class="sidebar--item sidebar--item-personal">
                    <span class="sidebar--label">Permiso de conducir</span>
                    <br id="permisoconducir"><?= $data['permisoconducir'] ?>
                </li>

                <li class="sidebar--item sidebar--item-personal">
                    <span class="sidebar--label">Página web</span>
                    <br id="pagweb"> <?= $data['pagweb'] ?>
                </li>

            </ul>

        </div>

        <div class="sidebar--data">
            <h2 class="sidebar--title">
                Idiomas
            </h2>

            <ul class="sidebar--list">
                <li class="sidebar--item">
                    <span class="sidebar--label"><?=$data['idioma']?></span>
                    <span class="sidebar--item-align-right">
                                <ul class="skills__list">
                                    <li class="skills__item active"></li>
                                    <li class="skills__item active"></li>
                                    <li class="skills__item "></li>
                                    <li class="skills__item "></li>
                                    <li class="skills__item "></li>
                                </ul>
                            </span>
                </li>
            </ul>

        </div>

    </aside>

    <section class="sections">

        <div class="section section--summary">

            <div class="section--content">
                <p id="resumen"><?= $data['resumen'] ?> </p>
            </div>
        </div>

        <div class="section">

            <div class="section--title">
                <h2>
                    Experiencia laboral
                </h2>
            </div>

            <!--
            <ol class="section--list">
                <li class="section--item">

                    <div class="section--heading-group">
                        <div class="section--date">sep 2018 - jul 2018</div>
                        <div class="section--heading">
                            <h3>CEO</h3>
                        </div>
                        <div class="section--sub-heading">
                            Disney </div>
                    </div>
                    <div class="section--content">
                        <p><strong>CEO </strong>y <strong>FIN</strong>﻿</p>
                    </div>
                </li>
            </ol>
            -->
        </div>

        <div class="section">
            <div class="section--title">
                <h2>
                    Estudios y certificaciones
                </h2>
            </div>

<!--
            <ol class="section--list">

                <li class="section--item">
                    <div class="section--heading-group">
                        <div class="section--date">
                            sep 2018 - jul 2018
                        </div>
                        <div class="section--heading">
                            <h3>Ingeniería en Sistemas Computacionales</h3>
                        </div>
                        <div class="section--sub-heading">
                            IPN, CDMX </div>
                    </div>
                    <div class="section--content">
                        <p>Conocieron los tabasqueños muchas historias de Andrés Manuel López Obrador, cómo sus experiencias juveniles allá por la selva Lacandona cuando se dejó crecer el pelo y era afecto a fumar marihuana y haber sostenido relaciones sexuales
                            en sus años mozos con una yegua llamada La Canica. En pocas palabras practicaba la zoofilia, una yegua con extraordinarias grupas, propiedad de un tal tío Lucio. De acuerdo a los datos recopilados por Blas A. Buendía, el testimonio
                            periodístico del diario Tabasco al Día, fechado el 21 de mayo de 1996 en Tepetitán, Macuspana, reza en una de sus partes: "Los amores de cierto personaje de la política que se ostenta como guía moral del Partido Revolución
                            Democrática, y una soyenca yegua propiedad del tío Lucio a la que se le conoció con el mote de La Canica, es un idilio del que pormenores el compañero Azarías Mendoza Guzmán y forma parte de toda una truculenta realidad".</p>
                        <ul>
                            <li>Extracto de una nota publicada en El Diario Tabasco al Día, 21 de mayo de 1996.</li>
                        </ul>
                        <p>"Tal vez por la rabia que le ocasionó esa asquerosidad, don Lucio León, al paso de tiempo enfermó hasta que desafortunadamente, según se logró averiguar, falleció".</p>
                        <ul>
                            <li>Testimonio de un familiar de Don Lucio León "Tío Lucio"</li>
                        </ul>
                        <p>
                            <br>
                        </p>
                    </div>
                </li>

            </ol>
            -->
        </div>

        <div class="section">

            <div class="section--title">
                <h2>
                    Destrezas
                </h2>
            </div>

<!--
            <ol class="section--list skills">

                <li class="section--item skills">
                    <div class="skills--label">
                        <h3>Paja a mano cambiada</h3>
                    </div>
                    <ul class="skills__list">
                        <li class="skills__item active"></li>
                        <li class="skills__item active"></li>
                        <li class="skills__item active"></li>
                        <li class="skills__item active"></li>
                        <li class="skills__item active"></li>
                    </ul>
                </li>

            </ol>
            -->

        </div>

    </section>

</body>

</html>
