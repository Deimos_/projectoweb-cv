<?php
session_start();
$data = $_SESSION['data'];
?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <link href="otago/css.css" rel="stylesheet">

</head>

<body class="c--ababab" style="overflow: hidden !important;">

    <div class="circle"></div>

    <div class="hero"></div>

    <section class="sections">

        <div class="section__label">
            CV
        </div>

        <div class="section--name">
            <h1>

                <div class="section--name__avatar" style="background:url('https://cvmaker-storage.s3.eu-west-3.amazonaws.com/img/avatar/d723a41edf4361cd4a1a00a55a021e73eca5eaa3.jpg') no-repeat;"></div>

                <span class="has--avatar">
                <?=$data['n1']?> <?=$data['a1']?>
                            </span>
            </h1>

            <span class="section--name__sub">
           <svg class="svg--address" fill="#000000" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
               <path d="M10 20v-6h4v6h5v-8h3L12 3 2 12h3v8z"></path>
               <path d="M0 0h24v24H0z" fill="none"></path>
           </svg>
             <?=$data['direccion']?>, <?=$data['ciudad']?> <svg class="svg--phone" fill="#000000" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                <path d="M0 0h24v24H0z" fill="none"></path>
                <path d="M6.62 10.79c1.44 2.83 3.76 5.14 6.59 6.59l2.2-2.2c.27-.27.67-.36 1.02-.24 1.12.37 2.33.57 3.57.57.55 0 1 .45 1 1V20c0 .55-.45 1-1 1-9.39 0-17-7.61-17-17 0-.55.45-1 1-1h3.5c.55 0 1 .45 1 1 0 1.25.2 2.45.57 3.57.11.35.03.74-.25 1.02l-2.2 2.2z"></path>
           </svg>
           <?=$data['telefono']?>
           <svg class="svg--email" fill="#000000" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
               <path d="M20 4H4c-1.1 0-1.99.9-1.99 2L2 18c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V6c0-1.1-.9-2-2-2zm0 4l-8 5-8-5V6l8 5 8-5v2z"></path>
               <path d="M0 0h24v24H0z" fill="none"></path>
           </svg>
           <?=$data['email']?>
        </span>

        </div>

        <div class="section__personalia">

            <ul class="personalia__list">

                <li class="personalia__item">
                    <b>Fecha de nacimiento</b>
                    <br> <?=$data['fenac']?>
                </li>

                <li class="personalia__item">
                    <b>Nacionalidad</b>
                    <br> <?=$data['nacionalidad']?>
                </li>

                <li class="personalia__item">
                    <b>Género</b>
                    <br> <?=$data['genero']?>
                </li>

                <li class="personalia__item">
                    <b>Página web</b>
                    <br> <?=$data['pagweb']?>
                </li>

                <li class="personalia__item">
                    <b>Lugar de nacimiento</b>
                    <br> <?=$data['lugnac']?>
                </li>

                <li class="personalia__item">
                    <b>Estado civil</b>
                    <br> <?=$data['estadocivil']?>
                </li>

                <li class="personalia__item">
                    <b>Permiso de conducir</b>
                    <br> <?=$data['permisoconducir']?>
                </li>

            </ul>

        </div>

        <div class="section section--summary">
            <div class="section--content">
                <p><?=$data['resumen']?></p>
            </div>
        </div>

        <div class="section">

            <div class="section--title">
                <h2>
                    <span class="section--title-label"></span> Experiencia laboral
                </h2>
            </div>

<!--
            <ol class="section--list">
                <li class="section--item">
                    <div class="section--heading-group">
                        <div class="section--date">sep 2018 - jul 2018</div>
                        <div class="section--heading">
                            <h3>CEO</h3>
                        </div>
                        <div class="section--sub-heading">
                            Disney, Tel Aviv </div>
                    </div>
                    <div class="section--content">
                        <p><strong>CEO </strong>y mas <strong>CEO</strong>﻿</p>
                    </div>
                </li>
            </ol>
            -->
        </div>

        <div class="section">

            <div class="section--title">
                <h2>
                    <span class="section--title-label"></span> Estudios y certificaciones
                </h2>
            </div>

<!--
            <ol class="section--list">

                <li class="section--item">
                    <div class="section--heading-group">
                        <div class="section--date">
                            sep 2018 - jul 2018
                        </div>
                        <div class="section--heading">
                            <h3>Ingeniería en Sistemas Computacionales</h3>
                        </div>
                        <div class="section--sub-heading">
                            IPN, CDMX </div>
                    </div>
                    <div class="section--content">
                        <p>Conocieron los tabasqueños muchas historias de Andrés Manuel López Obrador, cómo sus experiencias juveniles allá por la selva Lacandona cuando se dejó crecer el pelo y era afecto a fumar marihuana y haber sostenido relaciones sexuales
                            en sus años mozos con una yegua llamada La Canica. En pocas palabras practicaba la zoofilia, una yegua con extraordinarias grupas, propiedad de un tal tío Lucio. De acuerdo a los datos recopilados por Blas A. Buendía, el testimonio
                            periodístico del diario Tabasco al Día, fechado el 21 de mayo de 1996 en Tepetitán, Macuspana, reza en una de sus partes: "Los amores de cierto personaje de la política que se ostenta como guía moral del Partido Revolución
                            Democrática, y una soyenca yegua propiedad del tío Lucio a la que se le conoció con el mote de La Canica, es un idilio del que pormenores el compañero Azarías Mendoza Guzmán y forma parte de toda una truculenta realidad".</p>
                        <ul>
                            <li>Extracto de una nota publicada en El Diario Tabasco al Día, 21 de mayo de 1996.</li>
                        </ul>
                        <p>"Tal vez por la rabia que le ocasionó esa asquerosidad, don Lucio León, al paso de tiempo enfermó hasta que desafortunadamente, según se logró averiguar, falleció".</p>
                        <ul>
                            <li>Testimonio de un familiar de Don Lucio León "Tío Lucio"</li>
                        </ul>
                        <p>
                            <br>
                        </p>
                    </div>
                </li>
            </ol>
-->
        </div>

        <div class="section">

            <div class="section--title">
                <h2>
                    <span class="section--title-label"></span> Destrezas
                </h2>
            </div>
<!--
            <ol class="section--list skills">

                <li class="section--item skills">
                    <div class="skills--label">
                        <h3>
                            Paja a mano cambiada
                        </h3>
                    </div>
                    <div class="skills--label">

                        Muy bueno </div>
                </li>

            </ol>
            -->

        </div>

        <div class="section">

            <div class="section--title">
                <h2>
                    <span class="section--title-label"></span> Idiomas
                </h2>
            </div>

            <ol class="section--list skills">

                <li class="section--item skills">
                    <div class="skills--label">
                        <h3><?=$data['idioma']?>
                        </h3>
                    </div>
                    <div class="skills--label">
                        Medio </div>
                </li>
                <!--
                <li class="section--item skills">
                    <div class="skills--label">
                        <h3>Grocerias
                        </h3>
                    </div>
                    <div class="skills--label">
                        Nativo </div>
                </li>
                -->

            </ol>

        </div>

    </section>

</body>

</html>