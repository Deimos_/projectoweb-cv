<?php
session_start();
$data = $_SESSION['data'];
?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="format-detection" content="telephone=no">
    <meta http-equiv="Cache-Control" content="no-cache">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Expires" content="0">
    <meta http-equiv="Cache-Control" content="no-cache">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Expires" content="0">

    <link href="cambridge/css.css" rel="stylesheet">

</head>

<body class="c--3f6592 " style="overflow: hidden !important;">

    <div class="sidebar--bg"></div>
    <div class="hero--bg"></div>

    <aside class="sidebar">

        <div class="avatar" style="background:url('https://cvmaker-storage.s3.eu-west-3.amazonaws.com/img/avatar/d723a41edf4361cd4a1a00a55a021e73eca5eaa3.jpg') no-repeat;">&nbsp;
        </div>

        <div class="sidebar--data">
            <h2 class="sidebar--title">
                <span class="sidebar--title-icon">
                <svg fill="#fff" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                    <path d="M12 12c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm0 2c-2.67 0-8 1.34-8 4v2h16v-2c0-2.66-5.33-4-8-4z"></path>
                    <path d="M0 0h24v24H0z" fill="none"></path>
                </svg>
            </span> Personal
            </h2>

            <ul class="sidebar--list">
                <li class="sidebar--item">
                    <span class="sidebar--label">Nombre</span>
                    <br> <?=$data['n1']?> <?=$data['a1']?>
                </li>
                <li class="sidebar--item">
                    <span class="sidebar--label">Dirección</span>
                    <br> <?=$data['direccion']?>
                    <br> <?=$data['ciudad']?></li>
                <li class="sidebar--item">
                    <span class="sidebar--label">Número de teléfono</span>
                    <br> <?=$data['telefono']?>
                </li>
                <li class="sidebar--item">
                    <span class="sidebar--label">Correo electrónico</span>
                    <br> <?=$data['email'] ?>
                </li>

                <li class="sidebar--item">
                    <span class="sidebar--label">Fecha de nacimiento</span>
                    <br> <?= $data['fenach'] ?>
                </li>

                <li class="sidebar--item">
                    <span class="sidebar--label">Lugar de nacimiento</span>
                    <br> <?=$data['lugnac']?>
                </li>

                <li class="sidebar--item">
                    <span class="sidebar--label">Género</span>
                    <br> <?=$data['genero']?>
                </li>

                <li class="sidebar--item">
                    <span class="sidebar--label">Nacionalidad</span>
                    <br> <?=$data['nacionalidad'] ?>
                </li>

                <li class="sidebar--item">
                    <span class="sidebar--label">Estado civil</span>
                    <br> <?=$data['estadocivil']?>
                </li>

                <li class="sidebar--item">
                    <span class="sidebar--label">Permiso de conducir</span>
                    <br> <?=$data['permisoconducir']?>
                </li>

                <li class="sidebar--item">
                    <span class="sidebar--label">Página web</span>
                    <br> <?=$data['pagweb']?>
                </li>

            </ul>

        </div>

        <div class="sidebar--data">
            <h2 class="sidebar--title">
                <span class="sidebar--title-icon language">
                            <svg fill="#000000" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                <path d="M0 0h24v24H0z" fill="none"></path>
                                <path d="M11.99 2C6.47 2 2 6.48 2 12s4.47 10 9.99 10C17.52 22 22 17.52 22 12S17.52 2 11.99 2zm6.93 6h-2.95c-.32-1.25-.78-2.45-1.38-3.56 1.84.63 3.37 1.91 4.33 3.56zM12 4.04c.83 1.2 1.48 2.53 1.91 3.96h-3.82c.43-1.43 1.08-2.76 1.91-3.96zM4.26 14C4.1 13.36 4 12.69 4 12s.1-1.36.26-2h3.38c-.08.66-.14 1.32-.14 2 0 .68.06 1.34.14 2H4.26zm.82 2h2.95c.32 1.25.78 2.45 1.38 3.56-1.84-.63-3.37-1.9-4.33-3.56zm2.95-8H5.08c.96-1.66 2.49-2.93 4.33-3.56C8.81 5.55 8.35 6.75 8.03 8zM12 19.96c-.83-1.2-1.48-2.53-1.91-3.96h3.82c-.43 1.43-1.08 2.76-1.91 3.96zM14.34 14H9.66c-.09-.66-.16-1.32-.16-2 0-.68.07-1.35.16-2h4.68c.09.65.16 1.32.16 2 0 .68-.07 1.34-.16 2zm.25 5.56c.6-1.11 1.06-2.31 1.38-3.56h2.95c-.96 1.65-2.49 2.93-4.33 3.56zM16.36 14c.08-.66.14-1.32.14-2 0-.68-.06-1.34-.14-2h3.38c.16.64.26 1.31.26 2s-.1 1.36-.26 2h-3.38z"></path>
                            </svg>
                        </span> Idiomas
            </h2>

            <ul class="sidebar--list">
                <li class="sidebar--item">
                    <span class="sidebar--label"><?=$data['idioma']?></span>
                    <span class="sidebar--item-align-right">
                                        <i class="sidebar--item-level active"></i>
                                        <i class="sidebar--item-level active"></i>
                                        <i class="sidebar--item-level "></i>
                                        <i class="sidebar--item-level "></i>
                                        <i class="sidebar--item-level "></i>
                                    </span>
                </li>
                <!--
                <li class="sidebar--item">
                    <span class="sidebar--label">Grocerias</span>
                    <span class="sidebar--item-align-right">
                                        <i class="sidebar--item-level active"></i>
                                        <i class="sidebar--item-level active"></i>
                                        <i class="sidebar--item-level active"></i>
                                        <i class="sidebar--item-level active"></i>
                                        <i class="sidebar--item-level active"></i>
                                    </span>
                </li>
                -->
            </ul>
        </div>

    </aside>

    <section class="sections">

        <div class="section--name">
            <h1>
                Curriculum Vitae

            </h1>
        </div>

        <div class="section">

            <div class="section--title">
                <h2>
                    <hr>
                    <span>
                            Perfil
                        </span>
                </h2>
            </div>

            <div class="section--content section--content-single-textarea">
                <p><?=$data['resumen']?></p>
            </div>
        </div>

        <div class="section">

            <div class="section--title">
                <h2>
                    <hr>
                    <span>Experiencia laboral</span>

                </h2>
            </div>

            <div class="section--time-line-overlay"></div>

<!--
            <ol class="section--list">
                <li class="section--item">
                    <div class="section--time-line">
                        <i class="section--time-line-dot"></i>
                    </div>
                    <div class="section--time-line-hide-overlay"></div>
                    <div class="section--heading-group">
                        <div class="section--date">sep 2018 - jul 2018</div>
                        <div class="section--heading">
                            <h3>CEO</h3>
                        </div>
                        <div class="section--sub-heading">
                            Disney, Tel Aviv </div>
                    </div>
                    <div class="section--content">
                        <p><strong>CEO </strong>y mas <strong>CEO</strong>﻿</p>
                    </div>
                </li>
            </ol>
            -->
        </div>

        <div class="section">
            <div class="section--title">
                <h2>
                    <hr>
                    <span>
                                Estudios y certificaciones
                            </span>

                </h2>
            </div>

            <div class="section--time-line-overlay"></div>

<!--
            <ol class="section--list">

                <li class="section--item">
                    <div class="section--time-line">
                        <i class="section--time-line-dot"></i>
                    </div>
                    <div class="section--time-line-hide-overlay"></div>
                    <div class="section--heading-group">
                        <div class="section--date">
                            sep 2018 - jul 2018
                        </div>
                        <div class="section--heading">
                            <h3>Ingeníera en Sistemas Computacionales</h3>
                        </div>
                        <div class="section--sub-heading">
                            IPN, CDMX </div>
                    </div>
                    <div class="section--content">
                        <p>Conocieron los tabasqueños muchas historias de Andrés Manuel López Obrador, cómo sus experiencias juveniles allá por la selva Lacandona cuando se dejó crecer el pelo y era afecto a fumar marihuana y haber sostenido relaciones sexuales
                            en sus años mozos con una yegua llamada La Canica. En pocas palabras practicaba la zoofilia, una yegua con extraordinarias grupas, propiedad de un tal tío Lucio. De acuerdo a los datos recopilados por Blas A. Buendía, el testimonio
                            periodístico del diario Tabasco al Día, fechado el 21 de mayo de 1996 en Tepetitán, Macuspana, reza en una de sus partes: "Los amores de cierto personaje de la política que se ostenta como guía moral del Partido Revolución
                            Democrática, y una soyenca yegua propiedad del tío Lucio a la que se le conoció con el mote de La Canica, es un idilio del que pormenores el compañero Azarías Mendoza Guzmán y forma parte de toda una truculenta realidad".</p>
                        <ul>
                            <li>Extracto de una nota publicada en El Diario Tabasco al Día, 21 de mayo de 1996.</li>
                        </ul>
                        <p>"Tal vez por la rabia que le ocasionó esa asquerosidad, don Lucio León, al paso de tiempo enfermó hasta que desafortunadamente, según se logró averiguar, falleció".</p>
                        <ul>
                            <li>Testimonio de un familiar de Don Lucio León "Tío Lucio"</li>
                        </ul>
                        <p>
                            <br>
                        </p>
                    </div>
                </li>

            </ol>
            -->
        </div>

        <div class="section">

            <div class="section--title">
                <h2>
                    <hr>
                    <span>Destrezas</span>
                </h2>
            </div>

<!--
            <ol class="section--list skills">

                <li class="section--item skills">
                    <div class="skills--label">
                        <h3>Paja a mano cambiada</h3>
                    </div>
                    <div class="skills--stars">

                        <i class="skills--level active"></i>
                        <i class="skills--level active"></i>
                        <i class="skills--level active"></i>
                        <i class="skills--level active"></i>
                        <i class="skills--level active"></i>
                    </div>
                </li>

            </ol>
            -->

        </div>

    </section>

</body>

</html>