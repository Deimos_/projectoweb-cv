<?php
session_start();
$data = $_SESSION['data'];
?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="format-detection" content="telephone=no">
    <meta http-equiv="Cache-Control" content="no-cache">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Expires" content="0">

    <link href="berkeley/css.css" rel="stylesheet">

</head>

<body class="c--333333" style="overflow: hidden !important;">

    <div class="hero"></div>

    <section class="sections">

        <div class="section--name has--avatar">

            <div class="section--avatar" style="background: url('https://cvmaker-storage.s3.eu-west-3.amazonaws.com/img/avatar/d723a41edf4361cd4a1a00a55a021e73eca5eaa3.jpg') no-repeat;">
                &nbsp;
            </div>

            <h1> 
            <?= $data['n1']  ?>
            <?= $data['a1']  ?>
            </h1>
        </div>

        <div class="section">
            <div class="section--title">
                <h2>
                    <i class="section--title-icon personal">
                    <svg fill="#000000" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                        <path d="M0 0h24v24H0z" fill="none"></path>
                        <path d="M16 11c1.66 0 2.99-1.34 2.99-3S17.66 5 16 5c-1.66 0-3 1.34-3 3s1.34 3 3 3zm-8 0c1.66 0 2.99-1.34 2.99-3S9.66 5 8 5C6.34 5 5 6.34 5 8s1.34 3 3 3zm0 2c-2.33 0-7 1.17-7 3.5V19h14v-2.5c0-2.33-4.67-3.5-7-3.5zm8 0c-.29 0-.62.02-.97.05 1.16.84 1.97 1.97 1.97 3.45V19h6v-2.5c0-2.33-4.67-3.5-7-3.5z"></path>
                    </svg>
                </i> Personal
                </h2>
            </div>

            <ol class="section--list">
                <li class="section--item section--item-personal">
                    <div class="section--heading-group">
                        <div class="section--date">
                            Nombre
                        </div>
                        <div class="section--heading">
                            <h3> 
                                <?= $data['n1']  ?>
                                <?= $data['a1']  ?>
                            </h3>
                        </div>
                    </div>
                </li>
                <li class="section--item section--item-personal">
                    <div class="section--heading-group">
                        <div class="section--date">
                            Dirección
                        </div>
                        <div class="section--heading">
                            <h3><?=$data['direccion']?>, <?=$data['ciudad']?></h3>
                        </div>
                    </div>
                </li>
                <li class="section--item section--item-personal">
                    <div class="section--heading-group">
                        <div class="section--date">
                            Número de teléfono
                        </div>
                        <div class="section--heading">
                            <h3><?=$data['telefono']?></h3>
                        </div>
                    </div>
                </li>
                <li class="section--item section--item-personal">
                    <div class="section--heading-group">
                        <div class="section--date">
                            Correo electrónico
                        </div>
                        <div class="section--heading">
                            <h3><?=$data['email']?></h3>
                        </div>
                    </div>
                </li>

                <li class="section--item section--item-personal">
                    <div class="section--heading-group">
                        <div class="section--date">
                            Fecha de nacimiento
                        </div>
                        <div class="section--heading">
                            <h3> <?=$data['fenac']?>
                            </h3>
                        </div>
                    </div>
                </li>

                <li class="section--item section--item-personal">
                    <div class="section--heading-group">
                        <div class="section--date">
                            Lugar de nacimiento
                        </div>
                        <div class="section--heading">
                            <h3><?=$data['lugnac']?></h3>
                        </div>
                    </div>
                </li>

                <li class="section--item section--item-personal">
                    <div class="section--heading-group">
                        <div class="section--date">
                            Género
                        </div>
                        <div class="section--heading">
                            <h3> <?=$data['genero']?>
                            </h3>
                        </div>
                    </div>
                </li>

                <li class="section--item section--item-personal">
                    <div class="section--heading-group">
                        <div class="section--date">
                            Nacionalidad
                        </div>
                        <div class="section--heading">
                            <h3><?=$data['nacionalidad']?></h3>
                        </div>
                    </div>
                </li>

                <li class="section--item section--item-personal">
                    <div class="section--heading-group">
                        <div class="section--date">
                            Estado civil
                        </div>
                        <div class="section--heading">
                            <h3> <?= $data['estadocivil']?>
                            </h3>
                        </div>
                    </div>
                </li>

                <li class="section--item section--item-personal">
                    <div class="section--heading-group">
                        <div class="section--date">
                            Permiso de conducir
                        </div>
                        <div class="section--heading">
                            <h3> <?=$data['permisoconducir']?></h3>
                        </div>
                    </div>
                </li>

                <li class="section--item section--item-personal">
                    <div class="section--heading-group">
                        <div class="section--date">
                            Página web
                        </div>
                        <div class="section--heading">
                            <h3><?=$data['pagweb']?></h3>
                        </div>
                    </div>
                </li>

            </ol>

        </div>

        <div class="section">

            <div class="section--title">
                <h2>
                    <i class="section--title-icon single-textarea">
                        <svg fill="#000000" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                            <path d="M14 17H4v2h10v-2zm6-8H4v2h16V9zM4 15h16v-2H4v2zM4 5v2h16V5H4z"></path>
                            <path d="M0 0h24v24H0z" fill="none"></path>
                        </svg>
                    </i> Perfil

                </h2>
            </div>

            <div class="section--content section--content-single-textarea">
                <p><?=$data['resumen']?></p>
            </div>
        </div>

        <div class="section">

            <div class="section--title">
                <h2>
                    <i class="section--title-icon work">
                            <svg fill="#000000" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                <path d="M0 0h24v24H0zm10 5h4v2h-4zm0 0h4v2h-4z" fill="none"></path>
                                <path d="M10 16v-1H3.01L3 19c0 1.11.89 2 2 2h14c1.11 0 2-.89 2-2v-4h-7v1h-4zm10-9h-4.01V5l-2-2h-4l-2 2v2H4c-1.1 0-2 .9-2 2v3c0 1.11.89 2 2 2h6v-2h4v2h6c1.1 0 2-.9 2-2V9c0-1.1-.9-2-2-2zm-6 0h-4V5h4v2z"></path>
                            </svg>
                        </i> Experiencia laboral
                </h2>
            </div>

<!--
            <ol class="section--list">
                <li class="section--item">
                    <div class="section--heading-group">
                        <div class="section--date">sep 2018 - jul 2018</div>
                        <div class="section--heading">
                            <h3>CEO</h3>
                        </div>
                        <div class="section--sub-heading">
                            Disney, Tel Aviv </div>
                    </div>
                    <div class="section--content">
                        <p><strong>Juden </strong>y mas <strong>Juden</strong>﻿</p>
                    </div>
                </li>
            </ol>
            -->
        </div>

        <div class="section">
            <div class="section--title">
                <h2>
                    <i class="section--title-icon education">
                            <svg fill="#000000" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                <path d="M0 0h24v24H0z" fill="none"></path>
                                <path d="M5 13.18v4L12 21l7-3.82v-4L12 17l-7-3.82zM12 3L1 9l11 6 9-4.91V17h2V9L12 3z"></path>
                            </svg>
                        </i> Estudios y certificaciones

                </h2>
            </div>
            <!--
            <ol class="section--list">

                <li class="section--item">
                    <div class="section--heading-group">
                        <div class="section--date">
                            sep 2018 - jul 2018
                        </div>
                        <div class="section--heading">
                            <h3>doctorado en valer verga</h3>
                        </div>
                        <div class="section--sub-heading">
                            Harvard, Chilpan </div>

                        <div class="section--content">
                            <p>Conocieron los tabasqueños muchas historias de Andrés Manuel López Obrador, cómo sus experiencias juveniles allá por la selva Lacandona cuando se dejó crecer el pelo y era afecto a fumar marihuana y haber sostenido relaciones
                                sexuales en sus años mozos con una yegua llamada La Canica. En pocas palabras practicaba la zoofilia, una yegua con extraordinarias grupas, propiedad de un tal tío Lucio. De acuerdo a los datos recopilados por Blas A. Buendía,
                                el testimonio periodístico del diario Tabasco al Día, fechado el 21 de mayo de 1996 en Tepetitán, Macuspana, reza en una de sus partes: "Los amores de cierto personaje de la política que se ostenta como guía moral del Partido
                                Revolución Democrática, y una soyenca yegua propiedad del tío Lucio a la que se le conoció con el mote de La Canica, es un idilio del que pormenores el compañero Azarías Mendoza Guzmán y forma parte de toda una truculenta
                                realidad".</p>
                            <ul>
                                <li>Extracto de una nota publicada en El Diario Tabasco al Día, 21 de mayo de 1996.</li>
                            </ul>
                            <p>"Tal vez por la rabia que le ocasionó esa asquerosidad, don Lucio León, al paso de tiempo enfermó hasta que desafortunadamente, según se logró averiguar, falleció".</p>
                            <ul>
                                <li>Testimonio de un familiar de Don Lucio León "Tío Lucio"</li>
                            </ul>
                            <p>
                                <br>
                            </p>
                        </div>
                    </div>
                </li>
            </ol>
            -->

        </div>

        <div class="section">

            <div class="section--title">
                <h2>
                    <i class="section--title-icon skill">
                            <svg fill="#000000" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                <path d="M0 0h24v24H0z" fill="none"></path>
                                <path d="M13 1.07V9h7c0-4.08-3.05-7.44-7-7.93zM4 15c0 4.42 3.58 8 8 8s8-3.58 8-8v-4H4v4zm7-13.93C7.05 1.56 4 4.92 4 9h7V1.07z"></path>
                            </svg>
                        </i> Destrezas
                </h2>
            </div>

<!--
            <ol class="section--list skills">

                <li class="section--item">
                    <div class="section--heading-group">
                        <div class="section--heading">
                            <h3>Paja a mano cambiada
                            </h3>
                        </div>

                        <div class="section--sub-heading">Muy bueno</div>
                    </div>
                </li>

            </ol>
            -->

        </div>

        <div class="section">

            <div class="section--title">
                <h2>
                    <div class="section--title-icon language">
                        <svg fill="#000000" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                <path d="M0 0h24v24H0z" fill="none"></path>
                                <path d="M11.99 2C6.47 2 2 6.48 2 12s4.47 10 9.99 10C17.52 22 22 17.52 22 12S17.52 2 11.99 2zm6.93 6h-2.95c-.32-1.25-.78-2.45-1.38-3.56 1.84.63 3.37 1.91 4.33 3.56zM12 4.04c.83 1.2 1.48 2.53 1.91 3.96h-3.82c.43-1.43 1.08-2.76 1.91-3.96zM4.26 14C4.1 13.36 4 12.69 4 12s.1-1.36.26-2h3.38c-.08.66-.14 1.32-.14 2 0 .68.06 1.34.14 2H4.26zm.82 2h2.95c.32 1.25.78 2.45 1.38 3.56-1.84-.63-3.37-1.9-4.33-3.56zm2.95-8H5.08c.96-1.66 2.49-2.93 4.33-3.56C8.81 5.55 8.35 6.75 8.03 8zM12 19.96c-.83-1.2-1.48-2.53-1.91-3.96h3.82c-.43 1.43-1.08 2.76-1.91 3.96zM14.34 14H9.66c-.09-.66-.16-1.32-.16-2 0-.68.07-1.35.16-2h4.68c.09.65.16 1.32.16 2 0 .68-.07 1.34-.16 2zm.25 5.56c.6-1.11 1.06-2.31 1.38-3.56h2.95c-.96 1.65-2.49 2.93-4.33 3.56zM16.36 14c.08-.66.14-1.32.14-2 0-.68-.06-1.34-.14-2h3.38c.16.64.26 1.31.26 2s-.1 1.36-.26 2h-3.38z"></path>
                            </svg>
                    </div>
                    Idiomas
                </h2>
            </div>

            <ol class="section--list skills">

                <li class="section--item">
                    <div class="section--heading-group">
                        <div class="section--heading">
                            <h3><?=$data['idioma']?></h3>
                        </div>
                        <div class="section--sub-heading">Medio</div>
                    </div>
                </li>

<!--
                <li class="section--item">
                    <div class="section--heading-group">
                        <div class="section--heading">
                            <h3>Grocerias</h3>
                        </div>
                        <div class="section--sub-heading">Nativo</div>
                    </div>
                </li>
                -->

            </ol>

        </div>

    </section>

</body>

</html>