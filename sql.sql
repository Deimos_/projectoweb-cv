create table usuario (
		username varchar(50),
		password varchar(50),
		user_data int,

		primary key (username),
		foreign key (user_data) references user_data(user_data)
		);

create table user_data (
		user_data int not null auto_increment,
		avatar blob,
		nombres varchar(70),
		apellidos varchar(70),
		direccion varchar(100),
		ciudad varchar(50),
		telefono varchar(25),
		email varchar(75),
		fenac date,
		lugnac varchar(75),
		genero boolean,
		nacionalidad varchar(50),
		estadocivil varchar(25),
		permisoconducir varchar(10),
		pagweb varchar(35),
		idiomas varchar(100),
		resumen varchar(140)

		primary key(user_data),

		);


-- select para el inicio de sesion
select * from usuario where username = ? and password = ?;

-- select para obtener datos de usuario
select ud.avatar, ud.nombres, ud.apellidos, ud.direccion, ud.ciudad, ud.telefono,
	   ud.email. ud.fenac, ud.lugnac, ud.genero, ud.nacionalidad, ud.estadocivil,
	   ud.permisoconducir, ud.pagweb, ud.idiomas, ud.resumen
	   from user_data ud, usuario u
	   where u.user_data = ud.user_data and
			 u.username = ?;

-- select para crear nuevo usuario
insert into user_data (avatar, nombres, apellidos, direccion, ciudad,
					   telefono, email, fenac, lugnac, genero, nacionalidad,
					   estadocivil, permisoconducir, pagweb, idioas, resumen)
	values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);


insert into usuario (username, password, user_data) 
	values (?,?,
			(select * from user_data order by user_data desc limit 0,1)
			);
